/*
  Example: clusterplot_highlight($('.clusterplot svg'), 'red');

  On hover, highlights clusterplot labels in red.
 */

function clusterplot_highlight(selector, color) {
  function bisect(a, x) {
    var lo = 0, hi = a.length;
    while (lo < hi) {
      var mid = lo + hi >>> 1;
      if (a[mid] < x) lo = mid + 1; else hi = mid;
    }
    return lo;
  }

  selector.each(function() {
    var pos = $(this).position();
    var rects = $('.highlight rect', this);
    var diagonal = $('g.diagonal', this),
      xpos = diagonal.data('x').split(','),
      ypos = diagonal.data('y').split(',');
    $(this).on('mousemove', function(e) {
      var x = bisect(xpos, e.pageX - pos.left),
          y = bisect(ypos, e.pageY - pos.top);
      if ((x > 0) && (x < xpos.length - 1)) {
        rects.eq(0).attr({y: ypos[0], x: xpos[x-1], height: ypos[ypos.length - 1] - ypos[0], width : xpos[x] - xpos[x-1], fill: color });
        rects.eq(1).attr({x: xpos[0], y: ypos[x-1], width : xpos[xpos.length - 1] - xpos[0], height: ypos[x] - ypos[x-1], fill: color });
      }
      if ((y > 0) && (y < ypos.length)) {
        rects.eq(2).attr({y: ypos[0], x: xpos[y-1], height: ypos[ypos.length - 1] - ypos[0], width : xpos[y] - xpos[y-1], fill: color });
        rects.eq(3).attr({x: xpos[0], y: ypos[y-1], width : xpos[xpos.length - 1] - xpos[0], height: ypos[y] - ypos[y-1], fill: color });
      }
    }).on('mouseout', function() {
      rects.attr('fill', 'none');
    });
  });
}
